# Change Log

All notable changes to the "discordjs-simplifier" extension will be documented in this file.

## [1.2.0]

- Added snippets for sending message to channel or member, both work from **msg** prefix.

## [1.1.0]

- Added installation instructions to README.

## [1.0.1]

- Fixed cursor point at MessageEmbed snippet.
- Fixed README.md file.

## [1.0.0]

- Initial release of DiscordJS Simplifier snippets extension.