# discordjs-simplifier README

Simple snippets extension for VS Code, when you are programming your Discord.JS bots.

## Features

me - Adds MessageEmbed()

dise - Disable everyone mentioning

cmd - Command handler for your index-/app.js

!discordcmdhandler - Command handler for your handler/commands.js file

## Installation

Just simply go to [VS Marketplace](https://marketplace.visualstudio.com/items?itemName=bytebots.discordjs-simplifier) and follow their installation guide and you should be good to go. By default VS Code will also keep extensions up to date, so you will get all newest snippets to yourself automatically

## Known Issues

None at the moment

## Contributing

You can contribute to the extension by going to our [Gitlab page](https://gitlab.com/bytebots-botlab/discordjs-simplifier) and creating merge request or you can open issues and add "REQUEST" label to it. You can also join our [Discord Server](https://discord.gg/3fCGPjz)